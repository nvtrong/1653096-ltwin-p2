// Project1.cpp : Defines the entry point for the application.
//

//bug can't open then new and opposite
//open do but can't read data

#include "stdafx.h"
#include "Project1.h"


#include <commctrl.h>
#include"myPaint.h"

#include<fstream>

#define MAX_LOADSTRING 100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

TCHAR szDrawWnd[] = TEXT("DRAW");
HWND hwndMDIClient = NULL;
HWND hFrameWnd = NULL;
HWND hToolBarWnd = NULL;
HWND hCur = NULL;

HWND hOpen = NULL;

COLORREF rgbCurrentColor = RGB(0, 0, 0);

int nBitmapClass = 1;
int curCheck;

HIMAGELIST g_hImageList = NULL;

vector<ChildWdData> wndData;


int tmpx1, tmpx2;

//ChildWdData* childOpen = NULL;
int xs, ys;







// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam);

LRESULT onCommand(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK	inputTextProc(HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK	DrawWndProc(HWND, UINT, WPARAM, LPARAM);

void inputText(HWND hWnd, int x1, int x2);



//ChildWnd

void InitFrameWnd(HWND hWnd);
void initChildWnd(HWND hWnd, int nType);
void onNewDrawWnd(HWND hWnd);
void onMDIActivate(HWND hWnd, WPARAM wParam, LPARAM lParam);

//Setting
void initChoseColor(HWND hWnd);
void initChooseFont(HDC hdc, HWND hWnd);

//Toolbar
void crtToolBar(HWND hWnd);
void addDrawButton(HWND hWnd);

//Draw

void onLButtonUp(HWND hWnd, LPARAM lParam, int &x1, int &y1, int &x2, int &y2);
void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int &x1, int &y1, int &x2, int &y2);
void onChildPaint(HWND hWnd);


//File
void SaveFileDlg(HWND hWnd);
void SaveToFile(LPWSTR link);
void OpenDlg(HWND hWnd);
void OpenFile(HWND hWnd,LPWSTR link, OPENFILENAME opf);
//SelectObject

void SelectButDown(int xs, int ys, ChildWdData *wndData);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PROJECT1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJECT1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateMDISysAccel(hwndMDIClient, &msg) && !TranslateAccelerator(hFrameWnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PROJECT1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    RegisterClassExW(&wcex);


	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = DrawWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PROJECT1);
	wcex.lpszClassName = szDrawWnd;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	RegisterClassExW(&wcex);

	return RegisterClassExW(&wcex);

}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{

		InitFrameWnd(hWnd);
		crtToolBar(hWnd);
		addDrawButton(hWnd);
		return 0;

	}
	break;

	case WM_COMMAND:
	{
		onCommand(hWnd, message, wParam, lParam);
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
		SetWindowPos(hwndMDIClient, hwndMDIClient, 0, 32, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		ShowWindow(hwndMDIClient, SW_SHOW);
		return 0;
	}
	break;
	case WM_SIZE:
	{
		UINT x, y;
		x = LOWORD(lParam);
		y = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0, 0, x, y, TRUE);
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	break;
	case WM_MOVE:
	{

		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


LRESULT onCommand(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId = LOWORD(wParam);
	switch (wmId)
	{
	case ID_FILE_NEW:
	{
		onNewDrawWnd(hWnd);
	}
	break;
	case ID_WINDOW_TIDE:
		SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_VERTICAL, 0);
		break;
	case ID_WINDOW_CASCADE:
		SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0);
		break;
	case ID_WINDOW_CLOSEALL:
		EnumChildWindows(hwndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
		break;
	case ID_FILE_OPEN:
	{
		//MessageBox(hwndMDIClient, _T("Ban da chon Open"), _T("Open dialog"), 0);
		OpenDlg(hWnd);
	}
	break;
	case ID_FILE_SAVE:
	{
		//MessageBox(hwndMDIClient, _T("Ban da chon Save"), _T("Save dialog"), 0);

		SaveFileDlg(hCur);
	}
		
		break;
	case ID_DRAW_COLOR:
		initChoseColor(hCur);
		break;
	case ID_DRAW_FONT:
	{
		HDC hdc = GetDC(hCur);
		initChooseFont(hdc, hCur);
		ReleaseDC(hCur, hdc);
	}
	break;
	case ID_DRAW_ELLIPSE:
	case ID_DRAW_LINE:
	case ID_DRAW_RECTANGLE:
	case ID_DRAW_TEXT:
	case ID_DRAW_SELECTOBJECT:
	{
		HMENU hMenu = GetMenu(hWnd);
		CheckMenuItem(hMenu, curCheck, MF_UNCHECKED | MF_BYCOMMAND);
		curCheck = wmId;
		CheckMenuItem(hMenu, curCheck, MF_CHECKED | MF_BYCOMMAND);
	}
	break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	default:
		return DefMDIChildProc(hwndMDIClient, message, wParam, lParam);
	}
}



LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)(hChildWnd), 0L);
	return 1;
}


INT_PTR CALLBACK inputTextProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		EnableWindow(GetDlgItem(hWnd, IDC_IPTOK), FALSE);
		SetFocus(GetDlgItem(hWnd, IDC_TEXT));
		return INT_PTR(FALSE);
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_TEXT:
		{
			if (HIWORD(wParam) == EN_CHANGE)
				EnableWindow(GetDlgItem(hWnd, IDC_IPTOK), SendMessage((HWND)lParam, WM_GETTEXTLENGTH, 0, 0L));
		}
		break;
		case IDC_IPTCC:
		{
			EndDialog(hWnd, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
		case IDC_IPTOK:
		{
			ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);

			if (wndData == NULL) return INT_PTR(FALSE);


			Text *t = new Text;
			t->Color = wndData->Color;
			GetDlgItemText(hWnd, IDC_TEXT, t->str, MAX_TEXT_LENGTH);
			t->left = tmpx1;
			t->top = tmpx2;
			t->TextFont = wndData->Font;
			t->type = 4;
			wndData->data.push_back(t);
			HDC hdc = GetDC(hCur);

			HFONT hFont = CreateFontIndirect(&wndData->Font);
			SelectObject(hdc, hFont);
			SetTextColor(hdc, wndData->Color);

			TextOut(hdc, tmpx1, tmpx2, t->str, wcslen(t->str));
			ReleaseDC(hCur, hdc);
			DeleteObject(hFont);
			EndDialog(hWnd, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		default:
			return INT_PTR(FALSE);
		}
	}
	break;
	default:
		return INT_PTR(FALSE);
		break;
	}
	return INT_PTR(FALSE);
}

LRESULT CALLBACK	DrawWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int x1, y1, x2, y2;
	switch (message)
	{
	case WM_CREATE:
	{
		nBitmapClass++;
		initChildWnd(hWnd, 1);
	}
	break;

	case WM_CLOSE:
	{
		nBitmapClass--;
		SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)(hWnd), 0L);
	}
	break;
	case WM_MDIACTIVATE:
	{
		onMDIActivate(hWnd, wParam, lParam);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		x1 = x2 = LOWORD(lParam);
		y1 = y2 = HIWORD(lParam);
		if (curCheck == ID_DRAW_TEXT)
		{
			tmpx1 = x1;
			tmpx2 = y1;
			inputText(hWnd, x1, y1);
		}
		if (curCheck == ID_DRAW_SELECTOBJECT)
		{
			xs = x1;
			ys = y1;
			ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
			if (wndData->data.size() == 0)
			{
				MessageBox(hCur, _T("Don't have object to seclect"), _T("Notice"),0);
			}
			else
			{
				SelectButDown(xs, ys, wndData);
			}
				
		}

	}
	break;

	case WM_MOUSEMOVE:
		OnMouseMove(hWnd, wParam, lParam, x1, y1, x2, y2);
		break;
	case WM_LBUTTONUP:
	{
		onLButtonUp(hWnd, lParam, x1, y1, x2, y2);
	}
	break;
	case WM_PAINT:
	{
		onChildPaint(hWnd);
	}
	break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
}

void inputText(HWND hWnd, int x1, int x2)
{
	if (hWnd == NULL)
		return;
	DialogBox(NULL, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, inputTextProc);
}

void InitFrameWnd(HWND hWnd)
{
	hFrameWnd = hWnd;
	// Parse the menu selections:
	CLIENTCREATESTRUCT cs;
	cs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
	cs.idFirstChild = 500000;
	if (hWnd == NULL) {
		MessageBox(hWnd, _T("hWnd NULL"), _T("Null"), 0);
	}
	hwndMDIClient = CreateWindow(L"MDIClient",
		(LPCTSTR)NULL,
		WS_CHILD | WS_CLIPCHILDREN,
		0, 0, 0, 0,
		hWnd,
		(HMENU)NULL,
		hInst,
		(LPVOID)&cs);
	if (hwndMDIClient == NULL) {
		MessageBox(hWnd, _T("Loi"), _T("Loi"), 0);
	}
	ShowWindow(hwndMDIClient, SW_SHOW);


	HMENU hMenu = GetMenu(hWnd);
	CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED | MF_BYCOMMAND);
	curCheck = ID_DRAW_LINE;



}

void initChildWnd(HWND hWnd, int nType)
{
	ChildWdData* childData;
	childData = (ChildWdData*)VirtualAlloc(NULL, sizeof(ChildWdData), MEM_COMMIT, PAGE_READWRITE);
	childData->Color = RGB(0, 0, 0);
	ZeroMemory((&childData->Font), sizeof(LOGFONT));
	childData->Font.lfHeight = 20;
	wcscpy_s(childData->Font.lfFaceName, LF_FACESIZE, L"Arial");
	childData->hWnd = hWnd;
	childData->saved = false;
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)childData) == 0)
	{
		if (GetLastError() != 0)
		{
			int a = GetLastError();
			wchar_t buffer[256];
			wsprintfW(buffer, L"Error %d", a);
			MessageBox(hWnd, buffer, _T("fail set"), 0);
		}
	}





}

void onNewDrawWnd(HWND hWnd)
{
	MDICREATESTRUCT chld;
	chld.szClass = szDrawWnd;
	wsprintf(szTitle, L"Noname %d", nBitmapClass);
	chld.szTitle = szTitle;
	chld.hOwner = hInst;
	chld.x = CW_USEDEFAULT;
	chld.y = CW_USEDEFAULT;
	chld.cx = CW_USEDEFAULT;
	chld.cy = CW_USEDEFAULT;
	chld.style = 0;
	chld.lParam = 0;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&chld);

}

void onMDIActivate(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	ChildWdData *childData;

	HWND hActive = (HWND)LOWORD(lParam);
	childData = (ChildWdData*)GetWindowLongPtr(hWnd, 0);
	if (childData == NULL) return;
	hCur = hWnd;
}


void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int &x1, int &y1, int &x2, int &y2)
{
	if (!(wParam & MK_LBUTTON)) return;
	HDC hdc = GetDC(hWnd);
	switch (curCheck)
	{
	case ID_DRAW_LINE:
	{
		ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
		HPEN hPen = CreatePen(PS_SOLID, 1, wndData->Color);

		HDC hdc = GetDC(hWnd);
		SelectObject(hdc, hPen);
		SetROP2(hdc, R2_NOTXORPEN);
		MoveToEx(hdc, x1, y1, nullptr);
		LineTo(hdc, x2, y2);

		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);


		MoveToEx(hdc, x1, y1, nullptr);
		LineTo(hdc, x2, y2);
		ReleaseDC(hWnd, hdc);
	}
	break;
	case ID_DRAW_RECTANGLE:
	{
		ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
		HPEN hPen = CreatePen(PS_SOLID, 1, wndData->Color);
		HDC hdc = GetDC(hWnd);
		SetROP2(hdc, R2_NOTXORPEN);
		SelectObject(hdc, hPen);
		MoveToEx(hdc, x1, y1, nullptr);
		Rectangle(hdc, x1, y1, x2, y2);

		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);


		MoveToEx(hdc, x1, y1, nullptr);
		Rectangle(hdc, x1, y1, x2, y2);
		ReleaseDC(hWnd, hdc);
	}
	break;
	case ID_DRAW_ELLIPSE:
	{
		ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
		HPEN hPen = CreatePen(PS_SOLID, 1, wndData->Color);
		HDC hdc = GetDC(hWnd);
		SelectObject(hdc, hPen);
		SetROP2(hdc, R2_NOTXORPEN);
		MoveToEx(hdc, x1, y1, nullptr);
		Ellipse(hdc, x1, y1, x2, y2);

		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);


		MoveToEx(hdc, x1, y1, nullptr);
		Ellipse(hdc, x1, y1, x2, y2);
		ReleaseDC(hWnd, hdc);
	}
	break;
	}


	ReleaseDC(hWnd, hdc);
}

void onLButtonUp(HWND hWnd, LPARAM lParam, int &x1, int &y1, int &x2, int &y2)
{
	ChildWdData *wndData;
	wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
	if (wndData == NULL) return;

	switch (curCheck) {
	case ID_DRAW_LINE:
	{
		Line* l = new Line;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		l->Color = wndData->Color;
		l->type = 1;
		wndData->data.push_back(l);
	}
	break;
	case ID_DRAW_RECTANGLE:
	{

		Rect* l = new Rect;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		l->Color = wndData->Color;
		l->type = 2;
		wndData->data.push_back(l);
	}
	break;
	case ID_DRAW_ELLIPSE:
	{

		Elip* l = new Elip;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		l->Color = wndData->Color;
		l->type = 3;
		wndData->data.push_back(l);
	}
	break;
	}

}

void onChildPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL) return;
	if (wndData->data.size() != 0)
		for (int i = 0; i < wndData->data.size(); i++)
			wndData->data[i]->Draw(hdc);
	EndPaint(hWnd, &ps);
}


void initChoseColor(HWND hWnd)
{
	if (hWnd == NULL) return;
	CHOOSECOLOR cHc;
	COLORREF arcCustClr[16];

	ZeroMemory(&cHc, sizeof(CHOOSECOLOR));
	cHc.lStructSize = sizeof(CHOOSECOLOR);
	cHc.hwndOwner = hWnd;
	cHc.lpCustColors = (LPDWORD)arcCustClr;
	cHc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cHc))
	{
		HBRUSH hbrs;
		hbrs = CreateSolidBrush(cHc.rgbResult);
		rgbCurrentColor = cHc.rgbResult;
		ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
		wndData->Color = cHc.rgbResult;
	}
	else
		MessageBox(hwndMDIClient, _T("Can't open ChooseColor"), _T("Error"), 0);



}

void initChooseFont(HDC hdc, HWND hWnd)
{

	ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
	if (wndData == NULL) return;
	CHOOSEFONT cf;
	HFONT hFnew, hFOld;
	DWORD rgbPrev;
	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd;
	cf.lpLogFont = &wndData->Font;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS | CF_INITTOLOGFONTSTRUCT;
	if (ChooseFont(&cf))
	{
		hFnew = CreateFontIndirect(cf.lpLogFont);
		hFOld = (HFONT)SelectObject(hdc, hFnew);
		rgbPrev = SetTextColor(hdc, cf.rgbColors);

	}
	else
	{
		MessageBox(hwndMDIClient, _T("Error Choose Font"), _T("Error"), 0);
	}
}

void crtToolBar(HWND hWnd)
{
	InitCommonControls();


	TBBUTTON tbButtons[] =
	{
		{ STD_FILENEW, ID_FILE_NEW,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_FILEOPEN, ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0 , 0 },
	{ STD_FILESAVE, ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0 , 0 },
	};

	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));

}

void addDrawButton(HWND hWnd)
{
	TBBUTTON tbButtons[] =
	{
		{ 0, 0, TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	{ 0, ID_DRAW_LINE,TBSTATE_ENABLED,TBSTYLE_BUTTON,0,0 },
	{ 1, ID_DRAW_RECTANGLE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 2, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 3, ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0 },
	{ 4, ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0 }

	};

	TBADDBITMAP tbBitmap{ hInst, IDB_BITMAP7 };

	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);
	tbButtons[1].iBitmap += idx;
	tbButtons[2].iBitmap += idx;
	tbButtons[3].iBitmap += idx;
	tbButtons[4].iBitmap += idx;
	tbButtons[5].iBitmap += idx;
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);

}

void SaveFileDlg(HWND hWnd)
{
	if (hCur == NULL) return;
	OPENFILENAME svf;
	TCHAR szFile[MAX_LOADSTRING];
	TCHAR szFilter[] = TEXT("DrawFile\0*.drw\0");
	TCHAR duoi[] = TEXT("*.drw\0");
	wsprintf(szFile, L"%s",duoi);
	ZeroMemory(&svf, sizeof(OPENFILENAME));
	svf.lStructSize = sizeof(OPENFILENAME);
	svf.hwndOwner = hCur;
	svf.lpstrFilter = szFilter;
	svf.nFilterIndex = 1;
	svf.lpstrFile = szFile;
	svf.nMaxFile = sizeof(szFile);


	ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
	if (wndData == NULL) return;
	if (wndData->saved == false)
	{
		if (GetSaveFileName(&svf))
		{
			wsprintf(svf.lpstrFile, L"%s.drw", svf.lpstrFile);
			MessageBox(hCur, svf.lpstrFile, _T("Name file Saved"), 0);
			SetWindowText(hCur, svf.lpstrFile);
			//

			//
			SaveToFile(svf.lpstrFile);
		}
		else
		{
			MessageBox(hCur, _T("Error saving"), _T("Notice"), 0);
		}
	}
	else
	{
		wchar_t nameFile[MAX_LOADSTRING];
		GetWindowText(hCur, nameFile, wcslen(nameFile));
		SaveToFile(nameFile);
		MessageBox(hCur, nameFile, _T("Save to file"), 0);
	}
}

void SaveToFile(LPWSTR link)
{
	if (hCur == NULL) return;
	ChildWdData *wndData = (ChildWdData*)GetWindowLongPtr(hCur, 0);
	if (wndData == NULL) return;
	wndData->saved = true;
	std::fstream File;
	File.open(link, ios::out|ios::binary);
	if (!File)
	{
		MessageBox(hCur, _T("Open File to Save fail"), _T("Notice"), 0);
	}
	
	File.write((char*)&wndData->Font, sizeof(LOGFONT));
	File.write((char*)&wndData->Color, sizeof(COLORREF)); 
	int size = wndData->data.size();
	File.write((char*)&size,sizeof(size));
	for (int i = 0; i < size; i++)
	{
		if (wndData->data[i]->type == 1 || wndData->data[i]->type == 2 || wndData->data[i]->type == 3)
		{
			File.write((char*)(&wndData->data[i]->type),sizeof(int));
			File.write((char*)(&wndData->data[i]->left), sizeof(int));
			File.write((char*)(&wndData->data[i]->top), sizeof(int));
			File.write((char*)(&wndData->data[i]->right), sizeof(int));
			File.write((char*)(&wndData->data[i]->bottom), sizeof(int));
			File.write((char*)(&wndData->data[i]->Color), sizeof(COLORREF));
		}
		if (wndData->data[i]->type == 4)
		{
			Text * tmp = (Text*)wndData->data[i];
			File.write((char*)&wndData->data[i]->type, sizeof(int));
			File.write((char*)&wndData->data[i]->left, sizeof(int));
			File.write((char*)&wndData->data[i]->top, sizeof(int));
			File.write((char*)&tmp->TextFont, sizeof(LOGFONT));
			File.write((char*)&wndData->data[i]->Color, sizeof(COLORREF));
			File.write((char*)&tmp->str, sizeof(tmp->str));
		}
			

	}
}

void OpenDlg(HWND hWnd)
{
	//if (hCur == NULL) return;
	OPENFILENAME opf;
	TCHAR szFile[MAX_LOADSTRING];
	TCHAR szFilter[] = TEXT("DrawFile\0*.drw\0Text file\0*.txt\0Word file\0*.doc\0");
	szFile[0] = '\0';
	ZeroMemory(&opf, sizeof(OPENFILENAME));
	opf.lStructSize = sizeof(OPENFILENAME);
	opf.hwndOwner =hwndMDIClient;
	opf.lpstrFilter = szFilter;
	opf.nFilterIndex = 1;
	opf.lpstrFile = szFile;
	opf.nMaxFile = sizeof(szFile);
	opf.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	if (GetOpenFileName(&opf))
	{
		OpenFile(hWnd,opf.lpstrFile,opf);
	}
	else
	{
		MessageBox(hCur, _T("Error Open"), _T("Notice"), 0);
	}
}

void OpenFile(HWND hWnd,LPWSTR link, OPENFILENAME opf)
{
	//onNewDrawWnd(HWND);
	MDICREATESTRUCT chld;
	chld.szClass = szDrawWnd;
	chld.szTitle = link;
	chld.hOwner = hInst;
	chld.x = CW_USEDEFAULT;
	chld.y = CW_USEDEFAULT;
	chld.cx = CW_USEDEFAULT;
	chld.cy = CW_USEDEFAULT;
	chld.style = 0;
	chld.lParam = 0;
	hOpen = (HWND)SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&chld);
	

	ChildWdData* childData = (ChildWdData*)VirtualAlloc(NULL, sizeof(ChildWdData), MEM_COMMIT, PAGE_READWRITE);
	childData->saved = true;

	std::fstream File;
	File.open(link, ios::in|ios::binary );
	if (!File)
		return;
	File.read((char*)&childData->Font, sizeof(LOGFONT));
	File.read((char*)&childData->Color,sizeof(COLORREF));
	int numObject;
	File.read((char*)&numObject, sizeof(int));

	for (int i = 0; i < numObject; i++)
	{
		static int type;
		File.read((char*)&type, sizeof(int));
		switch (type) {
		case 1:
		{
			Line* l = new Line;
			File.read((char*)(&l->left), sizeof(int));
			File.read((char*)(&l->top), sizeof(int));
			File.read((char*)(&l->right), sizeof(int));
			File.read((char*)(&l->bottom), sizeof(int));
			File.read((char*)&l->Color, sizeof(COLORREF));
			l->type = 1;
			childData->data.push_back(l);
		}
		break;
		case 2:
		{

			Rect* l = new Rect;
			File.read((char*)&l->left, sizeof(int));
			File.read((char*)&l->top, sizeof(int));
			File.read((char*)&l->right, sizeof(int));
			File.read((char*)&l->bottom, sizeof(int));
			File.read((char*)&l->Color, sizeof(COLORREF));
			l->type = 2;
			childData->data.push_back(l);
		}
		break;
		case 3:
		{

			Elip* l = new Elip;
			File.read((char*)&l->left, sizeof(int));
			File.read((char*)&l->top, sizeof(int));
			File.read((char*)&l->right, sizeof(int));
			File.read((char*)&l->bottom, sizeof(int));
			File.read((char*)&l->Color, sizeof(COLORREF));
			l->type = 3;
			childData->data.push_back(l);
		}
		break;
		case 4:
		{
			Text* t = new Text;
			File.read((char*)&t->left, sizeof(int));
			File.read((char*)&t->top, sizeof(int));
			File.read((char*)&t->TextFont, sizeof(LOGFONT));
			File.read((char*)&t->Color, sizeof(COLORREF));
			File.read((char*)&t->str, sizeof(t->str));
			t->type = 4;
			childData->data.push_back(t);
		}
		break;
		}

	}
	SetLastError(0);
	if (SetWindowLongPtr(hCur, 0, (LONG_PTR)childData) == 0)
	{
		if (GetLastError() != 0)
		{
			int a = GetLastError();
			wchar_t buffer[256];
			wsprintfW(buffer, L"Error %d", a);
			MessageBox(hWnd, buffer, _T("fail set"), 0);
		}
	}
}


void SelectButDown(int xs, int ys, ChildWdData *wndData)
{
	for (int i = wndData->data.size() - 1; i >= 0; i--)
	{
		if (wndData->data[i]->left <= xs && xs <= wndData->data[i]->right
			&& wndData->data[i]->top <= ys && ys <= wndData->data[i]->bottom)
		{
			if (wndData->data[i]->type == 1)
			{
				MessageBox(hCur, _T("Line Selected"), _T("Test"), 0);
			}
			else
			{
				MessageBox(hCur, _T("Another Type"), _T("Test"), 0);
			}
		}
	}
}




























