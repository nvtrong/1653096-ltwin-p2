#pragma once
#include"stdafx.h"
#include<vector>
#include "resource.h"
#include <commctrl.h>
using namespace std;


#define MAX_TEXT_LENGTH 100
#define MAX_OBJECT 200






class Object
{
public:
	int type;
	int left, top, right, bottom;
	COLORREF Color;
	virtual void Draw(HDC hdc) = 0;
};

class Line :public Object
{
	public: void Draw(HDC hdc);
};

class Rect :public Object
{
	public: void Draw(HDC hdc);
};

class Elip :public Object
{
	public: void Draw(HDC hdc);
};

class Text :public Object
{

public:
	LOGFONT TextFont;
	wchar_t str[MAX_TEXT_LENGTH];
	void Draw(HDC hdc);
};

class ChildWdData
{
public:
	HANDLE hWnd;
	LOGFONT Font;
	COLORREF Color;
	vector<Object*> data;
	bool saved;

};

void Line::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, Color);
	SelectObject(hdc, hPen);
	MoveToEx(hdc, left, top, NULL);
	LineTo(hdc, right, bottom);
	DeleteObject(hPen);
}

void Text::Draw(HDC hdc)
{
	HFONT hFont = CreateFontIndirect(&TextFont);
	SelectObject(hdc, hFont);
	SetTextColor(hdc, Color);
	TextOut(hdc, left, top, str, wcsnlen(str, MAX_TEXT_LENGTH));
	DeleteObject(hFont);
}

void Rect::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, Color);
	SelectObject(hdc, hPen);
	Rectangle(hdc, left, top, right, bottom);
	

}

void Elip::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, Color);
	SelectObject(hdc, hPen);
	Ellipse(hdc, left, top, right, bottom);
}











