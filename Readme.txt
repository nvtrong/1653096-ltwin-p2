﻿Name: Nguyễn Văn Trọng.
ID: 1653096.
Mail: 1653096@student.hcmus.edu.vn
Phone: 01677668343

Build bằng Visual Studio 2017(x64)
Em đã cố gắng chia file nhưng trước khi code không chia nên sau này chia phát sinh lỗi mong thầy thông cảm.
Source lưu trữ trên Bitbucket tại: https://bitbucket.org/nvtrong/1653096-ltwin-p2/src/master/
Release đã có trong source.

Môn học Lập trình Windows (Windows Programming)

Bài tập project (P2)

Yêu cầu:
Cài đặt tiếp ứng dụng MyPaint của bài P1 theo các yêu cầu sau:
a. Khi user chọn menu item hay toolbar button thực hiện yêu cầu tương ứng theo các mô tả bên dưới : Đã hoàn thành
b. Color và Font:
o Color và Font được lưu trữ riêng cho mỗi child window, nghĩa là khi user vẽ trên child
window nào thì sẽ dùng color và font của child window đó: Đã hoàn thành
o Menu Color: user chọn màu trên Color dialog. Không yêu cầu set màu hiện hành là default
khi khởi tạo dialog: Đã hoàn thành.
o Menu Font: user chọn font, size, style (bold, italic, underline) trên Font dialog. Không yêu
cầu set font hiện hành là default khi khởi tạo dialog: Đã hoàn thành
c. Draw :
o Các object đã vẽ (line, rectangle, ellipse, text) được lưu trữ riêng cho mỗi child window : Đã hoàn thành
o Line: vẽ đoạn thẳng trên child window. Màu vẽ là màu đã chọn ở chức năng Color : Đã hoàn thành.
o Rectangle, Ellipse: tương tự như Line, nhưng vẽ hình chữ nhật hay ellipse : Đã hoàn thành
o Text: tạo một chuỗi text trên child window. Màu và font của text là màu và font đã chọn ở
chức năng Color, Font. Text được hiển thị tại vị trí click mouse. Tùy ý chọn lựa cách
nhập text: Đã hoàn thành
d. Paint lại child window : Đã hoàn thành.
o Cần vẽ lại các object cho child window khi nhận được message WM_PAINT.
e. Edit các object : Chưa hoàn thành
o Trong mode “Select object” (user chọn menu Draw/Select object), user có thể chỉnh sửa các
object bằng cách click mouse để chọn object và kéo/thả để chỉnh sửa kích thước hay di
chuyển vị trí của object.
f. Save : Đã hoàn thành
o Nếu child window chưa save (tên file mặc định=”Noname-*.drw”) thì mở dialog Save as để
user nhập tên file và lưu data của child window vào file. Gán tên file mới lên title của child
window. : Đã hoàn thành
o Nếu child window đã save rồi (tên file khác ”Noname-*.drw”) thì lưu data của child window
vào file : Đã hoàn thành
o Data cần lưu bao gồm: các object, color, font : Đã hoàn thành.
o Bạn được tùy ý chọn format (text, binary) và cấu trúc của file để lưu data: Đã hoàn thành.
g. Open : Đã hoàn thành
o Mở dialog Open để user chọn file *.drw : đã hoàn thành
o Đọc file và hiển thị các data lên child window :đã hoàn thành
o Gán tên file lên title của child window: đã hoàn thành
- Yêu cầu khác: project phải được lưu trữ trên repository của Bitbucket.
